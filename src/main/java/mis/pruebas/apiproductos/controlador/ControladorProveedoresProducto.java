package mis.pruebas.apiproductos.controlador;

import mis.pruebas.apiproductos.modelo.Producto;
import mis.pruebas.apiproductos.modelo.Proveedor;
import mis.pruebas.apiproductos.servicio.ServicioGenerico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;
import java.util.stream.Collectors;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/almacen/v1/productos/{idProducto}/proveedores")
public class ControladorProveedoresProducto {

    @Autowired
    ServicioGenerico<Producto> servicioProducto;

    @Autowired
    ServicioGenerico<Proveedor> servicioProveedor;

    @GetMapping
    public CollectionModel<EntityModel<Proveedor>> obtenerProveedoresProducto(@PathVariable long idProducto) {

        final List<Integer> idsProveedores = this.servicioProducto.getById(idProducto).getIdsProveedores();
        final List<Proveedor> pp = idsProveedores.stream().map(idProveedor -> this.servicioProveedor.getById(idProveedor)).collect(Collectors.toList());

        return CollectionModel.of(pp.stream().map(p -> EntityModel.of(p)
                .add(linkTo(methodOn(ControladorProveedor.class).getProviderById(p.getIdProveedor())).withSelfRel())
        ).collect(Collectors.toList()))
                .add(linkTo(methodOn(ControladorProducto.class).obtenerProductoPorId(idProducto)).withRel("producto"))
                .add(linkTo(methodOn(this.getClass()).obtenerProveedoresProducto(idProducto)).withSelfRel());
    }

    @GetMapping("/{indiceProveedor}")
    public Proveedor obtenerProveedoresProducto(@PathVariable long idProducto,
                                                @PathVariable int indiceProveedor) {
       final long idProveedor = this.servicioProducto
                .getById(idProducto)
                .getIdsProveedores().get(indiceProveedor);
        return this.servicioProveedor.getById(idProveedor);
    }

    static class ProveedorProducto {
        public int idProveedor;
    }

    @PostMapping
    public void addProviderProduct(@PathVariable long idProducto,
                                   @RequestBody ProveedorProducto provider) {
        try {
            this.servicioProveedor.getById(provider.idProveedor);
            this.servicioProducto.getById(idProducto).getIdsProveedores().add(provider.idProveedor);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{indiceProveedorProducto}")
    public void deleteProviderProd(@PathVariable long idProducto,
                                   @PathVariable int indiceProveedorProducto) {

        final List<Integer> listaIds = this.servicioProducto.getById(idProducto).getIdsProveedores();

        if (listaIds == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }else {
            try {
                listaIds.remove(indiceProveedorProducto);
            } catch (Exception x) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
}
