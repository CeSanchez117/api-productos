package mis.pruebas.apiproductos.controlador;

import mis.pruebas.apiproductos.modelo.Producto;
import mis.pruebas.apiproductos.servicio.ServicioGenerico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping("/almacen/v1/productos")
@ExposesResourceFor(Producto.class)
public class ControladorProducto {

    //Inyección de dependencias
    //notación que permite inyectar unas dependencias con otras dentro de Spring
    @Autowired
    ServicioGenerico<Producto> servicioProducto;

    @Autowired
    EntityLinks entityLinks;

    //Obtiene todos los productos que haya
    @GetMapping
    public CollectionModel<EntityModel<Producto>> obtenerProductos() {
        final Map<Long, Producto> productosMapa = this.servicioProducto.getAll();
        final List<Producto> productoList = new ArrayList<>();
        for(Long id: productosMapa.keySet()) {
            final Producto producto = productosMapa.get(id);
            producto.setId(id);
            productoList.add(producto);
        }

        //Lambdas, se pueden cambiar por métodos para recorrer
        return CollectionModel.of(
                productoList.stream().map(p -> obtenerRespuestaProducto(p)).collect(Collectors.toUnmodifiableList())
        ).add(linkTo(methodOn(this.getClass()).obtenerProductos()).withSelfRel());
    }

    /*hateoas Hypermedia As The Engine Of Application State
    dado un punto de entrada genérico de nuestra API REST,
    podemos ser capaces de descubrir sus recursos basándonos únicamente
     en las respuestas del servidor.*/
    private List<Link> crearEnlacesAdicionalesProducto(Producto p) {
        return Arrays.asList(crearEnlaceProducto(p),
                linkTo(methodOn(this.getClass()).obtenerProductos()).withRel("productos").withTitle("Todos los productos"),
                linkTo(methodOn(ControladorProveedoresProducto.class).obtenerProveedoresProducto(p.getId())).withRel("proveedores").withTitle("Lista de proveedores")
        );
    }

    private EntityModel<Producto> obtenerRespuestaProducto(Producto p) {
        return EntityModel.of(p).add(crearEnlacesAdicionalesProducto(p));
    }

    private Link crearEnlaceProducto(Producto p) {
        return this.entityLinks
                .linkToItemResource(p.getClass(), p.getId())
                .withSelfRel()
                .withTitle("Detalles de este producto");
    }

    @GetMapping("/{idProducto}")
    public EntityModel<Producto> obtenerProductoPorId(@PathVariable(name = "idProducto") long id) {
        try {
            final Producto p = this.servicioProducto.getById(id);
            return obtenerRespuestaProducto(p);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<EntityModel<Producto>> addProducto(@RequestBody Producto p) {
        final long id = this.servicioProducto.addProductos(p);
        p.setId(id);
        return ResponseEntity.ok().location(crearEnlaceProducto(p).toUri()).body(obtenerRespuestaProducto(p));
    }

    @PutMapping("/{idProducto}")
    public void updateById(@PathVariable(name = "idProducto") long id,
                           @RequestBody Producto p) {
        try {
            p.setId(id);
            this.servicioProducto.updateById(id, p);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    static class patchProducto {
        public Double precio;
        public Double cantidad;
    }

    @PatchMapping("/{idProducto}")
    public void patchProductoById(@PathVariable(name = "idProducto") long id,
                                  @RequestBody patchProducto productToPach) {
        final Producto prod = this.servicioProducto.getById(id);
            if (prod != null){
                if(productToPach.cantidad != null) {
                    prod.setCantidad(productToPach.cantidad);
                }else if(productToPach.precio != null) {
                    prod.setPrecio(productToPach.precio);
                }else{
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND);
                }
            }else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
    }

    @DeleteMapping("/{idProducto}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProductoPorId(@PathVariable(name = "idProducto") long id) {
        final Producto producto = this.servicioProducto.getById(id);

        if (producto == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }else{
            this.servicioProducto.deleteById(id);
        }
    }

}