package mis.pruebas.apiproductos.controlador;

import mis.pruebas.apiproductos.modelo.Proveedor;
import mis.pruebas.apiproductos.servicio.ServicioGenerico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/almacen/v1/proveedores")
public class ControladorProveedor {

    @Autowired
    ServicioGenerico<Proveedor> servicioProveedor;

    @GetMapping
    public List<Proveedor> getProviders() {
        final Map<Long, Proveedor> proveedorMap = this.servicioProveedor.getAll();
        final List<Proveedor> proveedorList = new ArrayList<>();

        for(Long idProveedor: proveedorMap.keySet()) {
            final Proveedor p = proveedorMap.get(idProveedor);
            p.setIdProveedor(idProveedor);
            proveedorList.add(p);
        }
        return proveedorList;
    }

    @GetMapping("/{idProveedor}")
    public Proveedor getProviderById(@PathVariable(name = "idProveedor") long id) {
        final Proveedor proveedor = this.servicioProveedor.getById(id);

        if (proveedor == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }else {
            return this.servicioProveedor.getById(id);
        }
    }

    @PostMapping
    public void addProvider(@RequestBody Proveedor p) {
        final long id = this.servicioProveedor.addProductos(p);
        p.setIdProveedor(id);
    }

    @PutMapping("/{idProveedor}")
    public void updateProviderById(@PathVariable(name = "idProveedor") long idProveedor,
                                   @RequestBody Proveedor proveedor) {
        try {
            this.servicioProveedor.updateById(idProveedor, proveedor);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{idProveedor}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProviderById(@PathVariable(name = "idProveedor") long id) {
        final Proveedor proveedor = this.servicioProveedor.getById(id);

        if (proveedor == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }else {
            this.servicioProveedor.deleteById(id);
        }
    }
}
