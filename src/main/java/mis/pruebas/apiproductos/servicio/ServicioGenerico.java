package mis.pruebas.apiproductos.servicio;

import java.util.Map;

public interface ServicioGenerico<T> {

    // CREATE
    public long addProductos(T t);

    // READ []
    public Map<Long, T> getAll();

    // READ
    public T getById(long id);

    // UPDATE
    public void updateById(long id, T t);

    // DELETE
    public void deleteById(long id);
}
