package mis.pruebas.apiproductos.servicio.servicioImpl;

import mis.pruebas.apiproductos.servicio.ServicioGenerico;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class ServicioGenericoImpl<T> implements ServicioGenerico<T> {
    private final AtomicLong secuenciaIds = new AtomicLong(0L);
    private final Map<Long, T> hashmap = new ConcurrentHashMap<>();

    @Override
    public long addProductos(T p) {
        final long id = secuenciaIds.incrementAndGet();
        this.hashmap.put(id, p);
        return id;
    }

    @Override
    public Map<Long, T> getAll() {
        return Map.copyOf(this.hashmap);
    }

    @Override
    public T getById(long id) {
        return this.hashmap.get(id);
    }

    @Override
    public void updateById(long id, T p) {
        if(this.hashmap.replace(id, p) == null) {
            throw new RuntimeException("No existe el T con Id. ".concat(String.valueOf(id)));
        }
    }

    @Override
    public void deleteById(long id) {
        final T t = this.hashmap.get(id);
        this.hashmap.remove(id, t);
    }
}
