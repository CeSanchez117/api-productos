package mis.pruebas.apiproductos;

import mis.pruebas.apiproductos.modelo.Producto;
import mis.pruebas.apiproductos.modelo.Proveedor;
import mis.pruebas.apiproductos.servicio.ServicioGenerico;
import mis.pruebas.apiproductos.servicio.servicioImpl.ServicioGenericoImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.ForwardedHeaderFilter;

@SpringBootApplication
public class ApiProductosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiProductosApplication.class, args);
	}

	// HTTP request -> [Filtro -> Filtro -> ForwardedHeaderFilter-> Filtro -> Filtro] -> HTTP response
	@Bean
	ForwardedHeaderFilter forwardedHeaderFilter() {
		return new ForwardedHeaderFilter();
	}

	@Bean
	ServicioGenerico<Proveedor> servicioProveedor() {
		return new ServicioGenericoImpl<Proveedor>();
	}

	@Bean
	ServicioGenerico<Producto> servicioProducto() {
		return new ServicioGenericoImpl<Producto>();
	}

}
