package mis.pruebas.apiproductos.modelo;

public class Proveedor {
    long idProveedor;
    String nombreProveedor;
    String direccionProveedor;

    public Proveedor(long id, String nombre, String direccion) {
        this.idProveedor = id;
        this.nombreProveedor = nombre;
        this.direccionProveedor = direccion;
    }

    public long getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(long idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public String getDireccionProveedor() {
        return direccionProveedor;
    }

    public void setDireccionProveedor(String direccionProveedor) {
        this.direccionProveedor = direccionProveedor;
    }
}